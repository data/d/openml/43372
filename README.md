# OpenML dataset: Hazardous-Driving-Spots-Around-the-World

https://www.openml.org/d/43372

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Content
This dataset identifies hazardous areas for driving according to harsh braking and accident level events within a specific area. Each month a new set of dangerous driving areas is produced and encapsulates one year of rolling data (i.e. from the previous month back 1 year). Associated with each area is a severity score that is based on the frequency of occurrences in the area and the severity of said occurrences. Data is aggregated over the previous 12 months.
Data
Some variables to point out:

SeverityScore: Severity score for each area as the number of harsh braking incidents and accident-level incidents for every 100 units of traffic flow. Traffic flow is defined as total hourly vehicle volume in the geohash.
IncidentsTotal: The total number of harsh braking incidents and accident-level events that have occurred within the geohash

Acknowledgements
This dataset is aggregated over the previous 12 months and is updated monthly. This data is publicly available from Geotab (geotab.com).
Inspiration
As some inspiration, here are some questions:

Which countries have the highest number of hazardous spots? Least number?
Can you create a dynamic geospatial visualization?

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43372) of an [OpenML dataset](https://www.openml.org/d/43372). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43372/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43372/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43372/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

